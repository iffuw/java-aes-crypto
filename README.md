# In general

This is my first publicly available repo. It consists of just one java file,
that should provide a simple standardized way to enrypt byte arrays with AES.
It is meant to be used for not too big arrays.
More elaborate methodology for handling larger arrays might be added in the future.

Any improvement suggestions are very welcome!

# General functionality

**Encryption**
For this task, at first salt [byte[8]] is generated with the secure RNG. An 
init vector [byte[16]] is created likewise. 
Those two are added to the output byte array in the order: salt, iv at offset (with plausibility checks).
After that, the array is encrypted in one step using the cipher.doFinal method.

**Decryption**
Can only decrypt arrays, that were ecrypted with the doEncryption method from this class.
It assumes, that 8 bytes from the input array at position offset comprise the salt, 
the next 16 the initializtion vector.
De decryption works pretty much like the encryption but backwards.