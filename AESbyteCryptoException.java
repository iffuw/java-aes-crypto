package Crypto;

public class AESbyteCryptoException extends Exception {
    // Constructor that accepts a message
    public AESbyteCryptoException(String message)
    {
        super(message);
    }
}
