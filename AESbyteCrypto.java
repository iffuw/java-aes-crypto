package Crypto;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

/**
 * A class to provide standardized functionality to encrypt and decrypt byte[] using AES with 256bit keys
 */
public class AESbyteCrypto {

    private final int KEY_LENGTH                         = 256;
    private final int ITERATION_COUNT                    = 65536;
    private final  String SECRET_KEY_FACTORY_PARAMETER   = "PBKDF2WithHmacSHA1";
    private final String ALGORITHM                       = "AES";
    private final String CIPHER_PARAMETER                = "AES/CBC/PKCS5Padding";
    private final SecureRandom secRand                   = new SecureRandom();
    //for hiding the salt and the init vector in the encrypted file
    private  int offset;

    public AESbyteCrypto(int offset) {
        this.offset = offset;
    }


    /**
     * Encrypt a given byte[] using AES with a 256 bit key.
     * @param password - String - The password that should be used for the encryption.
     * @param toEnc - byte[] - The byte array, that should be encrypted.
     * @return - byte[] - The encrypted byte array.
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws NoSuchPaddingException
     * @throws InvalidAlgorithmParameterException
     * @throws InvalidKeyException
     * @throws IOException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public byte[] doEncryption(String password, byte[] toEnc) throws AESbyteCryptoException {
        try {
            byte[] salt = new byte[8];
            //generate a salt
            //the salt is added to the password to add some degree of randomness
            SecureRandom secureRandom = new SecureRandom();
            secureRandom.nextBytes(salt);

            //generate the secret key from the given password using up-to-date standards (PKKDF2, SHA1)
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(SECRET_KEY_FACTORY_PARAMETER);
            KeySpec keySpec     = new PBEKeySpec(password.toCharArray(), salt, ITERATION_COUNT, KEY_LENGTH);
            SecretKey tmp       = secretKeyFactory.generateSecret(keySpec);
            SecretKey secret    = new SecretKeySpec(tmp.getEncoded(), ALGORITHM);

            //init a random initialization vector for better cipher (randomness to data)
            byte[] iv = new byte[16];
            secRand.nextBytes(iv);

            //init the cipher
            Cipher cipher       = Cipher.getInstance(CIPHER_PARAMETER);
            cipher.init(Cipher.ENCRYPT_MODE, secret, new IvParameterSpec(iv));
            //AlgorithmParameters params  = cipher.getParameters();

            //now encrypt the array
            byte[] encrypted   = cipher.doFinal(toEnc);

            //hide the salt and the iv in the encrypted array
            //they are stored inside the encrypted array at "offset"
            //in the order salt, iv
            byte[] encOut       = new byte[encrypted.length+salt.length+iv.length];
            //check if offset is possible
            checkOffset(encrypted);
            //part before salt and iv
            System.arraycopy(encrypted, 0, encOut, 0, offset);
            //salt
            System.arraycopy(salt, 0, encOut, offset, salt.length);
            //iv
            System.arraycopy(iv, 0, encOut, offset+salt.length, iv.length);
            //rest
            System.arraycopy(encrypted, offset, encOut, offset+salt.length+iv.length, encrypted.length-offset);

            return encOut;

        } catch(BadPaddingException |
                IllegalBlockSizeException |
                InvalidKeyException |
                InvalidAlgorithmParameterException |
                NoSuchAlgorithmException |
                NoSuchPaddingException |
                InvalidKeySpecException ioex){
            //none of these exceptions should kick in, since the parameters for the cipher and so on
            //are fixed and the relevant methods exist
            throw new AESbyteCryptoException("Internal encryption exception. This should not happen!");

        }
    }

    /**
     * Decrypts a byte[] encrypted with the do encryption method from this class.
     * @param password - String - The password used for the encryption.
     * @param toDec - byte[] - The byte array to decrypt.
     * @return - byte[] - The decrypted byte array.
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws NoSuchPaddingException
     * @throws InvalidAlgorithmParameterException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public byte[] doDecryption(String password, byte[] toDec) throws AESbyteCryptoException {
        try {
            //at first get the correct arrays
            byte[] salt         = new byte[8];
            byte[] iv           = new byte[16];
            byte[] toDecrypt    = new byte[toDec.length-salt.length-iv.length];
            //just to be sure, check the offset
            checkOffset(toDecrypt);
            //first part of the encrypted array
            System.arraycopy(toDec, 0, toDecrypt, 0, offset);
            //now the salt
            System.arraycopy(toDec, offset, salt, 0, salt.length);
            //now the iv
            System.arraycopy(toDec, offset+salt.length, iv, 0, iv.length);
            //now the rest of the encrypted array
            System.arraycopy(toDec, offset+salt.length+iv.length, toDecrypt, offset, toDecrypt.length-offset);

            //now recover the secret key
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(SECRET_KEY_FACTORY_PARAMETER);
            KeySpec keySpec     = new PBEKeySpec(password.toCharArray(), salt, ITERATION_COUNT, KEY_LENGTH);
            SecretKey tmp       = secretKeyFactory.generateSecret(keySpec);
            SecretKey secretKey = new SecretKeySpec(tmp.getEncoded(), ALGORITHM);

            //now init the cipher
            Cipher cipher       = Cipher.getInstance(CIPHER_PARAMETER);
            cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));

            //now decrypt the given byte[]
            //Since the files to be handled are rather small, this should suffice
            return cipher.doFinal(toDecrypt);

        } catch(BadPaddingException |
                IllegalBlockSizeException |
                InvalidKeyException |
                InvalidAlgorithmParameterException |
                NoSuchAlgorithmException |
                NoSuchPaddingException |
                InvalidKeySpecException ioex){
            //none of these exceptions should kick in, since the parameters for the cipher and so on
            //are fixed and the relevant methods exist
            throw new AESbyteCryptoException("Internal decryption exception. This should not happen!");
        }
    }

    private void checkOffset(byte[] givenArray){
        //offset can neither be bigger than the given array
        //nor it can be negative
        if(offset < 0)
            offset = 0;
        else if(offset > givenArray.length)
            offset  = givenArray.length;
    }
}




