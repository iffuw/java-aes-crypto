
import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

/**
 * A class to provide standardized functionality to encrypt and decrypt byte[] using AES with 256bit keys
 */
public class AESfileCrypto {

    private static final int KEY_LENGTH                         = 256;
    private static final int ITERATION_COUNT                    = 65536;
    private static final  String SECRET_KEY_FACTORY_PARAMETER   = "PBKDF2WithHmacSHA1";
    private static final String ALGORITHM                       = "AES";
    private static final String CIPHER_PARAMETER                = "AES/CBC/PKCS5Padding";
    private static final SecureRandom secRand                   = new SecureRandom();


    /**
     * Encrypt a given byte[] using AES with a 256 bit key.
     * @param password - String - The password that should be used for the encryption.
     * @param toEnc - byte[] - The byte array, that should be encrypted.
     * @return - byte[] - The encrypted byte array.
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws NoSuchPaddingException
     * @throws InvalidAlgorithmParameterException
     * @throws InvalidKeyException
     * @throws IOException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public static byte[] doEncryption(String password, byte[] toEnc) throws NoSuchAlgorithmException,
                                                                        InvalidKeySpecException,
                                                                        NoSuchPaddingException,
                                                                        InvalidAlgorithmParameterException,
                                                                        InvalidKeyException,
                                                                        IOException,
                                                                        BadPaddingException,
                                                                        IllegalBlockSizeException {
        byte[] salt     = new byte[8];
        //generate a salt andoutputFile store it
        //the salt is added to the password to add some degree of randomness
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextBytes(salt);

        //generate the secret key from the given password using up-to-date standards (PKKDF2, SHA1)
        SecretKeyFactory secretKeyFactory   = SecretKeyFactory.getInstance(SECRET_KEY_FACTORY_PARAMETER);
        KeySpec keySpec                     = new PBEKeySpec(password.toCharArray(), salt, ITERATION_COUNT, KEY_LENGTH);
        SecretKey tmp                       = secretKeyFactory.generateSecret(keySpec);
        SecretKey secret                    = new SecretKeySpec(tmp.getEncoded(), ALGORITHM);

        //init a random initialization vector for better cipher (randomness to data)
        //also store that to a file
        byte[] iv = new byte[16];
        secRand.nextBytes(iv);

        //init the cipher
        Cipher cipher           = Cipher.getInstance(CIPHER_PARAMETER);
        cipher.init(Cipher.ENCRYPT_MODE, secret, new IvParameterSpec(iv));
        //AlgorithmParameters params  = cipher.getParameters();

        //now encrypt the file and write output
        //at first write the salt and the init vector
        ByteArrayOutputStream encOut    = new ByteArrayOutputStream();
        encOut.write(salt);
        encOut.write(iv);
        //now encrypt the byteArray
        //call doFinal should suffice
        //since the files to be encrypted here are rather small
        //a more elaborate way ma be implemented in the future
        byte[] encrypted    = cipher.doFinal(toEnc);
        encOut.write(encrypted);

        return encOut.toByteArray();

    }

    /**
     * Decrypts a byte[] encrypted with the do encryption method from this class.
     * @param password - String - The password used for the encryption.
     * @param toDec - byte[] - The byte array to decrypt.
     * @return - byte[] - The decrypted byte array.
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws NoSuchPaddingException
     * @throws InvalidAlgorithmParameterException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public static byte[] doDecryption(String password, byte[] toDec) throws IOException,
                                                                        NoSuchAlgorithmException,
                                                                        InvalidKeySpecException,
                                                                        NoSuchPaddingException,
                                                                        InvalidAlgorithmParameterException,
                                                                        InvalidKeyException,
                                                                        BadPaddingException,
                                                                        IllegalBlockSizeException {
        //set up stream for better handling
        ByteArrayInputStream toDecrypt  = new ByteArrayInputStream(toDec);

        //at first the salt used for decryption must be read
        byte[] salt = toDecrypt.readNBytes(8);

        //the initialization vector must also be read
        byte[] iv = toDecrypt.readNBytes(16);

        //now recover the secret key
        SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(SECRET_KEY_FACTORY_PARAMETER);
        KeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, ITERATION_COUNT, KEY_LENGTH);
        SecretKey tmp = secretKeyFactory.generateSecret(keySpec);
        SecretKey secretKey = new SecretKeySpec(tmp.getEncoded(), ALGORITHM);

        //now init the cipher
        Cipher cipher = Cipher.getInstance(CIPHER_PARAMETER);
        cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));

        //now decrypt the given byte[]
        //therefore read all remaining bytes to decrypt them.
        //Since the files to be handled are rather small, this should suffice
        byte [] decrypted   = toDecrypt.readAllBytes();
        decrypted           = cipher.doFinal(decrypted);

        //return
        return decrypted;

    }


}




